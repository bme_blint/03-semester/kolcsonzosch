const express = require('express');
const router = require('./src/route/router');
const session = require('express-session');
const FileStore = require("session-file-store")(session);

const app = express();
app.set('view engine', 'ejs');

app.use(express.urlencoded({
    extended: true,
    limit: "20MB"
}));
app.use(express.json());

app.use(
    session({
        secret: 'verysecure',
        resave: false,
        saveUninitialized: true,
        store: new FileStore({})
    })
);

//load router
router(app);
app.use(express.static("static"));

const server = app.listen(3000, function () {
    console.log("Running on :3000");
})