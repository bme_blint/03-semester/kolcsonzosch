const requiredOption = require('../requireOption');

module.exports = function (objectrepository){
    const UserModel = requiredOption(objectrepository, 'UserModel');
    const ItemModel = requiredOption(objectrepository, 'ItemModel');

    return function (req,res, next){
        res.locals.item.borrowby = undefined;
        res.locals.item.borrowbythis = false;
        res.locals.item.save();
        return res.redirect('/');
    }
}