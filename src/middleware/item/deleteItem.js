const requiredOption = require('../requireOption');

module.exports = function (objectrepository) {
    return function (req, res, next) {
        if (typeof res.locals.item === 'undefined') {
            return next();
        }

        res.locals.item.remove(err => {
            if (err) {
                return next(err);
            }

            return res.redirect('/');
        });
    }
}
                ;;                ;;        ;;  ;;  ;;
     ;;;;;;    ;;    ;;;;;;      ;;  ;;        ;;  ;;
    ;;    ;;  ;;  ;;;;          ;;;;      ;;  ;;  ;;
   ;;    ;;  ;;      ;;;;      ;;  ;;    ;;  ;;  ;;
  ;;;;;;    ;;  ;;;;;;        ;;    ;;  ;;  ;;  ;;
 ;;
;;


   ;;;;;;  ;;;;      ;;;;
  ;;    ;;    ;;  ;;;;;;;;
 ;;    ;;    ;;  ;;
;;    ;;    ;;    ;;;;;;
