const requiredOption = require('../requireOption');

module.exports = function (objectrepository) {
    const ItemModel = requiredOption(objectrepository, 'ItemModel');
    
    return function (req, res, next) {
        if (
            typeof req.body.name === 'undefined' ||
            typeof req.body.organization === 'undefined' ||
            typeof req.body.location === 'undefined'
        ) {
            return next();
        }
        
        if (typeof res.locals.item === 'undefined' || typeof res.locals.item.save === "undefined") {
            res.locals.item = new ItemModel();
        }

        res.locals.item.name = req.body.name;
        res.locals.item.organization = req.body.organization;
        res.locals.item.location = req.body.location;
        res.locals.item.borrowby = undefined;

        res.locals.item.save(err => {
            if (err) {
                return next(err);
            }

            return res.redirect('/');
        })
    }
}