const requiredOption = require('../requireOption');
const orgs = require('../../db/organizations');

module.exports = function (objectrepository) {
    const ItemModel = requiredOption(objectrepository, 'ItemModel');

    return function (req, res, next) {
        ItemModel.findOne({ _id: req.params.id }, (err, item) => {
            if (err || !item) {
                return next(err);
            }

            res.locals.item = item;
            return next();
        })
    }
}