const requiredOption = require('../requireOption');

module.exports = function (objectrepository){
    const UserModel = requiredOption(objectrepository, 'UserModel');
    const ItemModel = requiredOption(objectrepository, 'ItemModel');

    return function (req,res, next){
        res.locals.item.borrowby = req.session.user._id;
        res.locals.item.save();
        return res.redirect('/');
    }
}