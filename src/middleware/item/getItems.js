const requiredOption = require('../requireOption');
const orgs = require('../../db/organizations');

module.exports = function (objectrepository) {
    const ItemModel = requiredOption(objectrepository, 'ItemModel');
    const UserModel = requiredOption(objectrepository, 'UserModel');

    return function (req, res, next) {
        ItemModel.find({}, (err, items) => {
            if (err) {
                return next(err);
            }

            if (items.length === 0) {
                res.locals.items = [];
                return next();
            }
            
            let toWait = items.length;
            
            for (let i = 0; i < items.length; i++){
                UserModel.findOne({ _id: items[i].borrowby }, (err, user) => {
                    if (!err && user) {
                        items[i].borrower = user;
                        if (typeof req.session.user !== 'undefined') {
                            if (user._id+"" === req.session.user._id) {
                                items[i].borrowbythis = true;
                            } else {
                                items[i].borrowbythis = false;
                            }
                        }
                    }
                    toWait--;
                    if (toWait === 0) {
                        res.locals.items = items;
                        return next();
                    }
                });
            }
        })
    }
}