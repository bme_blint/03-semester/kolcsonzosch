const requiredOption = require('../requireOption');

module.exports = function (objectrepository) {
    return function (req, res, next) {
        if (req.session.loggedin === true) {
            req.session.user = res.locals.user;
            req.session.save(err => {
             if (err){
                 return next(err);
             }
                return res.redirect('/');
            });
        }
    };
}