const requiredOption = require('../requireOption');

module.exports = function (objectrepository) {

    return function (req, res, next) {
        if (typeof req.body.password === 'undefined') {
            return next();
        }

        req.session.regenerate(err => {
            if (err !== null) {
                console.log(err);
                return ; // TODO: error kezelés
            }

            if (req.body.password === res.locals.user.pwhash) {
                req.session.loggedin = true;
                return req.session.save(err => {
                    if (err === null) {
                        return next();
                    }
                    console.log(err);
                });
            } else {
                res.locals.error = 'Hibás jelszó!';
                res.locals.user = undefined;
                return next();
            }

        });


    }
}