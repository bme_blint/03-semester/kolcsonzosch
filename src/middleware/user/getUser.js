const requiredOption = require('../requireOption');

module.exports = function (objectrepository) {
    const UserModel = requiredOption(objectrepository, 'UserModel');

    return function (req, res, next) {
        if (typeof req.session.user !== 'undefined'){
            res.locals.user = req.session.user;
            return next();
        }
        
        if (typeof req.body.email === 'undefined'){
            return next();
        }

        UserModel.findOne({ email: req.body.email }, (err, user) => {
            if (err || !user) {
                return next(err);
            }

            res.locals.user = user;
            return next();
        })
    }
}