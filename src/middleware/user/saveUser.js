const requiredOption = require('../requireOption');

module.exports = function (objectrepository) {
    const UserModel = requiredOption(objectrepository, 'UserModel');

    return function (req, res, next) {
        if (
            typeof req.body.lastname === 'undefined' ||
            typeof req.body.firstname === 'undefined' ||
            typeof req.body.email === 'undefined' ||
            typeof req.body.password === 'undefined'
        ) {
            return next();
        }

        if (typeof res.locals.user === 'undefined') {
            res.locals.user = new UserModel();
        }
        
        res.locals.user.lastname = req.body.lastname;
        res.locals.user.firstname = req.body.firstname;
        res.locals.user.email = req.body.email;
        res.locals.user.pwhash = req.body.password; //TODO: hash

        res.locals.user.save(err => {
            if (err) {
                return next(err);
            }
            
            req.session.user = res.locals.user;
            req.session.loggedin = true;
            req.session.save(err => {
                if (err) {
                    return next(err);
                }
                return next();
            });
        });
    }
}