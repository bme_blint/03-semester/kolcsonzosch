const requireOption = require('./requireOption');
const orgs = require('../db/organizations');

module.exports = function (objectrepository, viewName){
    return function (req, res) {
        let obj = res.locals;
        obj.session = req.session;
        obj.organizations = orgs;
        res.render(viewName, obj)
        console.log('render: ' + viewName);
    };
};