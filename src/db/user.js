const db = require("./db");

const User = db.model('User', {
    lastname: String,
    firstname: String,
    email: String,
    pwhash: String
});

module.exports = User;