const mongoose = require('mongoose');

let host = 'localhost';

if (typeof process.env.MONGO !== "undefined" && process.env.MONGO) {
    host = process.env.MONGO;
}

setTimeout(() => {
    mongoose.connect('mongodb://'+host+'/izum0b').then(() => {
        console.log(`Connected to ${host} successfully`);
    }).catch(e => {
        console.log(e);
    });
}, 1000);



module.exports = mongoose;