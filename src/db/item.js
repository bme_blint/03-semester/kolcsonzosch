const Schema = require('mongoose').Schema;
const db = require("./db");

const Item = db.model('Item', {
    name: String,
    organization: String,
    location: String,
    borrowby: {
        type: Schema.Types.ObjectID,
        ref: 'User'
    }
});

module.exports = Item;