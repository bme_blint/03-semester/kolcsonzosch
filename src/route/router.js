const auth      = require('../middleware/auth/auth');
const checkPass = require('../middleware/auth/checkPass');
const render    = require('../middleware/render');
const delItem   = require('../middleware/item/deleteItem');
const getItem   = require('../middleware/item/getItem');
const getItems  = require('../middleware/item/getItems');
const saveItem  = require('../middleware/item/saveItem');
const getUser   = require('../middleware/user/getUser');
const saveUser  = require('../middleware/user/saveUser');
const session   = require('../middleware/auth/session');
const logout    = require('../middleware/auth/logout');
const borrow    = require('../middleware/item/borrow');
const unborrow  = require('../middleware/item/unborrow');

const UserModel = require('../db/user');
const ItemModel = require('../db/item');

module.exports = function (app) {
    const objRepo = {
        UserModel : UserModel,
        ItemModel : ItemModel
    };

    app.get('/login',
        render(objRepo, 'login'));

    app.post('/login',
        getUser(objRepo),
        checkPass(objRepo),
        session(objRepo));

    app.get('/logout',
        logout(objRepo));

    app.get('/register',
        render(objRepo, 'register'));

    app.post('/register',
        saveUser(objRepo),
        session(objRepo));

    app.use('/new',
        auth(objRepo),
        saveItem(objRepo),
        render(objRepo, 'edit'));

    app.get('/edit/:id',
        auth(objRepo),
        getItem(objRepo),
        render(objRepo, 'edit'));

    app.post('/edit/:id',
        auth(objRepo),
        getItem(objRepo),
        saveItem(objRepo));

    app.get('/delete/:id',
        auth(objRepo),
        getItem(objRepo),
        delItem(objRepo));

    app.get('/useredit/',
        auth(objRepo),
        getUser(objRepo),
        render(objRepo, 'useredit'));

    app.post('/useredit/',
        auth(objRepo),
        getUser(objRepo),
        saveUser(objRepo),
        session(objRepo));

    app.get('/borrow/:id',
        auth(objRepo),
        getItem(objRepo),
        borrow(objRepo));

    app.get('/unborrow/:id',
        auth(objRepo),
        getItem(objRepo),
        unborrow(objRepo));

    app.get('/',
        getItems(objRepo),
        render(objRepo, 'index'));
}