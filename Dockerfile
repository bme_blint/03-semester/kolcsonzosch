FROM node:latest

WORKDIR /app

ADD src /app/src
ADD static/*.svg /app/static/
ADD static/*.css /app/static/
ADD views /app/views
ADD index.js /app/
ADD packag*.json /app/

RUN npm i

VOLUME /app/sessions
EXPOSE 3000

ENTRYPOINT ["node", "index.js"]
